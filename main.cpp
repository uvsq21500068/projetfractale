// Guillot Benjamin & Rodriguez de la Nava Lydia
#include <QApplication> //include necessaire pour l'utilisation de Qt
#include "AccueilProjet.h"  //fenêtre d'acceuil qui permettera de choisir entre les ensembles de Julia et mandelbrot/ OpenGL et Cairo
#include "intantanne.h" //include permettant d'afficher une fractale avec openGL
//#include "vect.h"

using namespace std;
	
int main(int argc, char *argv[])
{
    QApplication app(argc, argv); 

    Window fenetre;
    fenetre.show();

    return app.exec();
}