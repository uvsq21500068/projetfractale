// Guillot Benjamin & Rodriguez de la Nava Lydia
//permet d'avoir un rendu instantanne grâce à OpenGL
#include "intantanne.h"
#include <complex> //permet l'utilisation des nombres complexes 
#include <iostream>
#include <cmath>

using namespace std;

instantanne::instantanne(int p_nbite, double p_zmax, double p_cR, double p_cI, double p_granularite, QWidget *parent) 
    : dessin(60, parent, "Fractales avec OpenGL", p_nbite, p_zmax, p_cR, p_cI, p_granularite)
{
}

void instantanne::initialiser() 
{
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void instantanne::resizeGL(int width, int height) 
    if(height == 0)
        height = 1;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void instantanne::dessinerPoint(double Red, double Green, double Blue, complex<double> c){
    glBegin(GL_POINTS);
    glColor3f(Red,Green,Blue);
    glVertex3d(c.real(), c.imag(),0.0f);       //3eme arguments : ZOOM 
    glEnd();
}

void instantanne::keyPressEvent(QKeyEvent *keyEvent) 
{
    switch(keyEvent->key())
    {
        case Qt::Key_F1:
            close();
            break;
        case Qt::Key_Right:
            this->argTranslate1 -= 0.1f;
            break;
        case Qt::Key_Left:
            this->argTranslate1 += 0.1f;
            break;
        case Qt::Key_Up:
            this->argTranslate2 -= 0.1f;
            break;
        case Qt::Key_Down:
            this->argTranslate2 += 0.1f;
            break;
        case Qt::Key_F2:
            this->argTranslate3 += 0.02f;
            break;
        case Qt::Key_F3:
            this->argTranslate3 -= 0.02f;
            break;
        case Qt::Key_F4:
            this->granularite = this->granularite / 2;
            break;
    }
}

void instantanne::afficheFractale()
{
    complex<double> c, c2;
    int Red = 0, Green = 0, Blue = 0;
    int v_nbite = this->nbite; 
    double v_zmax = this->zmax;
    double v_cR = this->cR;
    double v_cI = this->cI;
    double v_granul = this->granularite;
    double ite = 0.0;
    bool mandelbrot = false;
    c.real(v_cR);
    c.imag(v_cI);

    if (v_cR == 0.0 && v_cI == 0.0)
        mandelbrot = true;

    for (double x = longueurMin; x < longueurMax; x += v_granul){
        for(double y = largeurMin; y < largeurMax; y += v_granul){

            c2.real(x);
            c2.imag(y);

            if (mandelbrot){
                c.real(x);
                c.imag(y);
            }

            ite = calculFractacles(c, v_nbite, v_zmax, x, y); //on récuppère le nombre d'itération pour chaques points  

            if(ite == -1){

                dessinerPoint(0.0,0.0,0.0,c2);            
            }
            else {

                Red = ((int)(2*ite*sqrt(ite)))%255;
                Green = ((int)(ite/2*255))/255;
                Blue = ((int)((2+ sin(1.0*ite))*ite)) % 255;
                dessinerPoint(Red, Green, Blue, c2);
            }
        }
    }
}

void instantanne::paintGL() //permet l'affichage de pixels a l'ecran
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(this->argTranslate1, this->argTranslate2, this->argTranslate3); //centre le referenciel au milieu de l'écran
    afficheFractale();
}





