// Guillot Benjamin & Rodriguez de la Nava Lydia
#ifndef INSTANTANE_H
#define INSTANTANE_H

#include "dessin.h" 

/*! \class instantanne

     La classe est lancée en cliquant sur le bouton openGL à l'accueil.
*/

class instantanne : public dessin
{
    //Q_OBJECT
public:
    //! Constructeur de la classe instantanne
    /*!
       
    */
    explicit instantanne(int p_nbite = 0, double p_zmax = 0, double p_cR = 0, double p_cI = 0, double p_granularite = 0, QWidget *parent = 0);
    //! Initialisation de la fenêtre de la fractale
    /*!
       Contient les fonctions openGL nécessaires pour la création d'une fenêtre de dessin.
    */
    void initialiser();
    //! Mise à jour de la taille de la fenêtre
    /*!
       Est appelée à chaque fois qu'on change la taille de la fenêtre.
    */
    void resizeGL(int width, int height); 
    //! Dessine le contenu de la fractale
    /*!
       Permet l'affichage de pixel à l'écran, centre le référentiel au milieu de l'écran et dessine la fractale.
       \param width la largeur de la fenêtre
       \param height la hauteur de la fenêtre
    */
    void paintGL(); 
    //! Dessiner un point coloré avec openGL
    /*!
       \param R la valeur de la couleur rouge
       \param V la valeur de la couleur verte
       \param B la valeur de la couleur bleu
       \param c le nombre complexe qui contient les coordonnées du point à dessiner
    */
    void dessinerPoint(double R, double V, double B, complex<double> c);
    //! Gestion des événements quand on appuie sur une touche
    /*!
       Translation de la fractale vers la droite si flèche droite, vers la gauche si flèche gauche,
       vers le bas si flèche bas, vers le haut si flèche haut, zoom avant si F2, zoom arrière si F3, plus de précision si F4.
    */
    void keyPressEvent( QKeyEvent *keyEvent );
    //! Affichage de la fractale
    /*!
       Récupère les valeurs entrées dans la fenêtre d'accueil, et crée la fractale avec ces valeurs.
       Pour chaque point de la fenêtre, on calcule le nombre d'itération avant que le module calculé
       à chaque itération ne dépasse zmax. C'est par rapport au nombre d'itération qu'on choisit la couleur.
       Si on fait le calcul sur l'ensemble de Julia&Fatou il faut qu'on garde la constante d'entrée.
    */
    void afficheFractale();

private:
	double argTranslate1 = 0.5f; //!<Initialisation de la valeur du mouvement pour se déplacer de gauche à droite ou de droite à gauche
	double argTranslate2 = 0.0f;//!<Initialisation de la valeur du mouvement pour se déplacer de haut en bas ou de bas en haut
	double argTranslate3 = -3.0f;//!<Initialisation de la valeur du mouvement pour le zoom
	double largeurMin = -1.0;//!<Initialisation de la valeur minimale de l'axe des abscisses
	double largeurMax = 1.0;//!<Initialisation de la valeur maximale de l'axe des abscisses
	double longueurMin = -2.0;//!<Initialisation de la valeur minimale de l'axe des ordonnées
	double longueurMax = 1.0;//!<Initialisation de la valeur maximale de l'axe des ordonnées
};

#endif 