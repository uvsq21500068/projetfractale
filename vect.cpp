// Guillot Benjamin & Rodriguez de la Nava Lydia
//rendu grâce à CAIRO
//#include "intantanne.h"
#include "vect.h"
#include <complex> //permet l'utilisation des nombres complexes 
#include <iostream>
#include <cmath>

using namespace std;

vect::vect()
{
    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 900, 600);
    cr = cairo_create (surface);
    cairo_scale (cr, 1, 1);
}



void vect::dessinerPoint(double R, double G, double B, complex<double> c){
	cairo_set_source_rgb (cr,R,G,B);			 
	cairo_rectangle (cr, c.real()+300, c.imag()+200,1,1); 		
	cairo_fill (cr);
}

double vect::calculFractacles(complex<double> c, int maxIte, double Zmax, double p_cR, double p_cI){
    double i=0;
    double x1, y1;
    double tmp;
    x1 = p_cR;
    y1 = p_cI;

    while (i < maxIte){
        if(fmod(x1,y1)>Zmax) return i; 
        tmp = x1;
        x1 = x1*x1-y1*y1 + c.real();
        y1 = 2*tmp*y1 + c.imag();
        i+=0.1;
    }
    return -1;
}

void vect::afficheFractale() //permet l'affichage de pixels a l'ecran
{
    complex<double> c, p;
    int v_nbite = this->nbite; 
    double v_zmax = this->zmax;
    double v_cR = this->cR;
    double v_cI = this->cI;
    double ite = 0.0;
    bool mandelbrot = false;
    c.real(v_cR);
    c.imag(v_cI);
    
    

    if (v_cR == 0.0 && v_cI == 0.0)
        mandelbrot = true;

    for (double x = -2.0; x < 1.0; x += 0.01){
        for(double y = -1.0; y < 1.0; y += 0.01){
            if (mandelbrot){ c.real(x); c.imag(y);}
            
            p.real(x*100); 
            p.imag(y*100);

            ite = calculFractacles(c, v_nbite, v_zmax, x, y); //on récuppère le nombre d'itération pour chaques points   
            assignerCouleur(ite, p);
        }
    }
   

    cairo_surface_write_to_png (surface, "fractale.svg");
}

void vect::assignerCouleur(int ite, complex<double> p){ 	//assigne la couleur et dessine le point
	int Red, Green, Blue;
	if(ite == -1.0){
        vect::dessinerPoint(30.0,40.0,50.0,p);            
    }
    else {
        Red = ((int)(2*ite*sqrt(ite)))%255;
        Green = ((int)(ite/2*255))/255;
        Blue = ((int)((2+ sin(1.0*ite))*ite)) % 255;
        vect::dessinerPoint(Red, Green, Blue, p);
    }
}
