// Guillot Benjamin & Rodriguez de la Nava Lydia
#include "AccueilProjet.h"
#include "intantanne.h" //include permettant d'afficher une fractale avec openGL
#include <QHBoxLayout>
#include <iostream>

Window::Window() : QWidget()
{
    setFixedSize(600, 400);

    // Construction du bouton
    m_Open = new QPushButton("OpenGL", this);
    m_Open->setFont(QFont("Comic Sans MS", 14));
    m_Open->setCursor(Qt::PointingHandCursor);
    QObject::connect(m_Open, SIGNAL(clicked()), this, SLOT(FractaleOpenGL())); 


    m_Cairo = new QPushButton("Cairo", this);
    m_Cairo->setFont(QFont("Comic Sans MS", 14));
    m_Cairo->setCursor(Qt::PointingHandCursor);

    m_quit = new QPushButton("Quit", this);
    m_quit->setFont(QFont("Comic Sans MS", 14));
    m_quit->setCursor(Qt::PointingHandCursor);
    QObject::connect(m_quit, SIGNAL(clicked()), qApp, SLOT(quit()));

    m_cReal->setDecimals(5);
    m_cReal->setMinimum(-2);
    m_cReal->setMaximum(1);
    m_cReal->setSuffix("\ \ \ 0 pour Mandelbrot");
    m_cImag->setDecimals(5);
    m_cImag->setMinimum(-1);
    m_cImag->setMaximum(1);
    m_cImag->setSuffix("\ \ \ 0 pour Mandelbrot");
    m_granularite->setDecimals(5);
    m_granularite->setMinimum(0);
    m_granularite->setMaximum(1);
    m_granularite->setSuffix("\ \ \ 0.01 default");
    QFormLayout *formLayout = new QFormLayout;
    formLayout->addRow("Real", m_cReal);        //a recuperer si on selectionne Julia&Fatou
    formLayout->addRow("Imag", m_cImag);        //a recuperer si on selectionne Julia&Fatou
    formLayout->addRow("Zmax", m_module);
    formLayout->addRow("Granularite", m_granularite);

    QGridLayout *layout = new QGridLayout;

    layout->addWidget(m_Cairo, 1, 0);
    layout->addWidget(m_Open, 2, 0);
    layout->addLayout(formLayout, 3, 0);
    layout->addWidget(m_quit, 4, 0);

    this->setLayout(layout);
        
}

void Window::FractaleOpenGL()
{   
    double module = m_module->value();
    double p_cR = m_cReal->value();
    double p_cI = m_cImag->value();
    double p_granularite = m_granularite->value();
    instantanne *versionOpenGL;
    versionOpenGL = new instantanne(10, module, p_cR, p_cI, p_granularite); 
    versionOpenGL->show();
}

