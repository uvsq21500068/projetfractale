// Guillot Benjamin & Rodriguez de la Nava Lydia
#ifndef DESSIN_H
#define DESSIN_H

// INCLUDE NECESSAIRES A UTILISATION OPENGL
#include <QtOpenGL>  
#include <QGLWidget>
#include "GL/glu.h"
#include <complex>

/*! \class dessin
    \brief La classe mère pour le dessin de la fractale

     Classe qui contient les classes virtuelles pour les classes filles, ainsi que le calcul de la valeur d'échappement
     de la fractale 

*/

using namespace std;
// QGLWidget necessaire pour l'utilisation d'opengl avec QT


class dessin : public QGLWidget 
{
    Q_OBJECT
public:
    //! COnstructeur de la classe
    /*!
       
    */
    explicit dessin(int fps= 0, QWidget *parent = 0, char *name = 0, int p_nbite = 0, double p_Zmax = 0, double p_cR = 0, double p_cI = 0, double p_granularite = 0); //FPS = nombre d'images/secondes, p_nbite = nombre d'iterations, p_zmax = module, p_cR et cI = nombre complexe
    //! Initialisation de la fractale
    /*!
       Méthode virtuelle
    */
    virtual void initialiser() = 0;
    //! Affichage de la fractale
    /*!
       Méthode virtuelle
    */
    virtual void afficheFractale() = 0;
    //! Quitter la fenêtre
    /*!
       Ferme la fenêtre quand on appuie sur echap.
    */
    virtual void keyPressEvent(QKeyEvent *keyEvent);
    //! Calcul du nombre d'itération d'une fractale
    /*!
       \param nc un nombre complexe
       \param maxIte le nombre entier max d'itération à effectuer dans le calcul
       \param Zmax la valeur du module, la valeur d'échappemment
       \param p_cR partie réelle de la constante de Julia&Fatou. 0 si on fait Mandelbrot
       \param p_cI partie imaginaire de la constante de Julia&Fatou. 0 si on fait Mandelbrot
       \return le nombre d'itérations si le module calculé a dépassé Zmax. -1 sinon.
    */
    virtual double calculFractacles (complex<double> nc, int maxIte, double Zmax, double p_cR, double p_cI);//max = nombre d'iterations
    //! Dessiner un point coloré
    /*!
       \param R la valeur de la couleur rouge
       \param V la valeur de la couleur verte
       \param B la valeur de la couleur bleu
       \param c le nombre complexe qui contient les coordonnées du point à dessiner
    */
    virtual void dessinerPoint(double R, double V, double B, complex<double> c)= 0;

        
public slots:
    //! Mise à jour de la fenêtre
    /*!
       Refait l'affichage en permanence. Permet de ne pas faire bugger l'agrandissement de la fenêtre.
    */
    virtual void timeOutSlot();

private:
    QTimer *t_Timer;

protected:
	int nbite; //!<Le nombre d'itération
    double zmax; //!<La valeur du module, la escape value
    double cR; //!<La partie réelle de la constante
    double cI; //!<La partie imaginaire de la constante
    double granularite; //!<Valeur entre 0 et 1 pour la précision du dessin


};


#endif 