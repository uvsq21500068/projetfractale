// Guillot Benjamin & Rodriguez de la Nava Lydia
#ifndef DEF_Window
#define DEF_Window
 
#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QLCDNumber>
#include <QSlider>
#include <QCheckBox>
#include <QDoubleSpinBox>

/*! \class Window
    \brief Une classe qui crée la fenêtre d'accueil du projet

    La classe Window crée une fenêtre qui permet à l'utilisateur de choisir les paramètres de la fractales.
    S'il choisit d'initialiser le nombre complexe à 0, le programme créera une fractale de Mandelbrot. Autrement,
    le paramètres sont récupérés pour former une fractale de Julia&Fatou.
    Il peut aussi choisir zmax ainsi que la précision (granularité) de la fractale.

*/

 
class Window : public QWidget 
{
	Q_OBJECT

    public:
    //! Création de la fenêtre de l'accueil
    /*!
      Crée les boutons de la fenêtre d'accueil pour choisir les paramètres ainsi que leur valeur minimum et maximum.
      La valeur du nombre réel doit être situé entre -2 et 1, l'image entre -1 et 1, la granularité en 0 et 1 et enfin
      le module doit être positif.
    */
    Window();

    private:
    QPushButton *m_Open; //!<widget affichant un bouton pour lancer openGL
    QPushButton *m_Cairo; //!<widget affichant un bouton pour lancr Cairo
    QPushButton *m_quit; //!<widget affichant un bouton pour fermer la fenêtre
    QDoubleSpinBox *m_cReal = new QDoubleSpinBox;   //!<widget qui permet d'entrer le nombre réel de la valeur de Julia&Fatou
    QDoubleSpinBox *m_cImag = new QDoubleSpinBox;  //!<widget qui permet d'entrer le nombre imaginaire de la valeur de Julia&Fatou
    QDoubleSpinBox *m_module = new QDoubleSpinBox; //!<widget pour récupérer zmax
    QDoubleSpinBox *m_granularite = new QDoubleSpinBox; //!<widget pour choisir la précision de la fractale


    
    public slots:
    //! Récupération des paramètres entrés
    /*!
      Récupère les valeurs de la fenêtre de lancement et lance la création de la fractale avec par défaut 100 itérations.
    */
    void FractaleOpenGL();
    
};
 
#endif