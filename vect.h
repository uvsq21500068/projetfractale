// Guillot Benjamin & Rodriguez de la Nava Lydia
#ifndef VECT__H
#define VECT__H

//#include "dessin.h" //classe abstraite mère 

#include "/usr/include/cairo/cairo.h"
#include <complex>

using namespace std;

/*! \class vect

     Classe qui permet l'affichage de la fractale de manière vectorielle grâce à openGL.
*/

class vect 
{
    cairo_t *cr;//!<Contient l'état courant de la fenêtre
    cairo_surface_t *surface; //!<Représente une image
    int nbite = 100; //!<Nombre d'itération initial
    double zmax = 2.0;//!<Module de valeur d'échappement
    double cR = 0.0;//!<Partie réelle de la constante à récupérer
    double cI = 0.0;//!<Partie imaginaire de la constant à récupérer

public:
    //! Constructeur de la classe
    /*!
       Crée la fenêtre
    */
	vect();
    //! Dessiner un point coloré
    /*!
       \param R la valeur de la couleur rouge
       \param V la valeur de la couleur verte
       \param B la valeur de la couleur bleu
       \param c le nombre complexe qui contient les coordonnées du point à dessiner
    */
    void dessinerPoint(double Red, double Green, double Blue, complex<double> c);
     //! Affichage de la fractale
    /*!
       Récupère les valeurs entrées dans la fenêtre d'accueil, et crée la fractale avec ces valeurs.
       Pour chaque point de la fenêtre, on calcule le nombre d'itération avant que le module calculé
       à chaque itération ne dépasse zmax. C'est par rapport au nombre d'itération qu'on choisit la couleur.
       Si on fait le calcul sur l'ensemble de Julia&Fatou il faut qu'on garde la constante d'entrée.
    */
    void afficheFractale();
     //! Choix de la couleur
    /*!
       On choisit la couleur par rapport au nombre d'itération
       \param it le nombre d'itération du point étudié
       \param p la valeur du point à dessiner
    */
    void assignerCouleur(int ite, complex<double> p);
    //! Calcul du nombre d'itération d'une fractale
    /*!
       \param nc un nombre complexe
       \param maxIte le nombre entier max d'itération à effectuer dans le calcul
       \param Zmax la valeur du module, la valeur d'échappemment
       \param p_cR partie réelle de la constante de Julia&Fatou. 0 si on fait Mandelbrot
       \param p_cI partie imaginaire de la constante de Julia&Fatou. 0 si on fait Mandelbrot
       \return le nombre d'itérations si le module calculé a dépassé Zmax. -1 sinon.
    */
    double calculFractacles (complex<double> nc, int maxIte, double Zmax, double p_cR, double p_cI);
};

#endif