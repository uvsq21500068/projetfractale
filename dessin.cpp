// Guillot Benjamin & Rodriguez de la Nava Lydia
#include "dessin.h"

using namespace std;

dessin::dessin(int fps, QWidget *parent, char *name, int p_nbite, double p_Zmax, double p_cR, double p_cI, double p_granularite) //yésépacommentcamarche
    : QGLWidget(parent)
{
    setWindowTitle(QString::fromUtf8(name)); //nom de la fenêtre à ouvrir
    nbite = p_nbite;
    zmax = p_Zmax;
    cR = p_cR;
    cI = p_cI;
    granularite = p_granularite;
    if(fps == 0)
        t_Timer = NULL;
    else
    {
        int seconde = 1000; // 1 seconde = 1000 ms
        int timerInterval = seconde / fps;
        t_Timer = new QTimer(this);
        connect(t_Timer, SIGNAL(timeout()), this, SLOT(timeOutSlot()));
        t_Timer->start( timerInterval );
    }
}

void dessin::keyPressEvent(QKeyEvent *keyEvent) 
{
    switch(keyEvent->key())
    {
        case Qt::Key_Escape:
            close();
            break;
    }
}

void dessin::timeOutSlot() 
{
    updateGL(); 
}

double dessin::calculFractacles(complex<double> c, int maxIte, double Zmax, double p_cR, double p_cI){
    double i=0;
    double x1, y1;
    double tmp;
    x1 = p_cR;
    y1 = p_cI;

    while (i < maxIte){
        if(fmod(x1,y1)>Zmax) return i; 
        tmp = x1;
        x1 = x1*x1-y1*y1 + c.real();
        y1 = 2*tmp*y1 + c.imag();
        i+=0.1;
    }
    return -1;
}


